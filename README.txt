WAYF (Where are you from)

See http://wayf.dk/en for details about becoming identity- or service-provider.

The SPorto SAML class was originally written by Mads Freek.
See https://code.google.com/p/sporto/ for details

Author: Hasse Ramlev Wilson <hasse@ramlev.dk>. Originally by Tom Helmer Hansen, tom@adapt.dk, 2013

