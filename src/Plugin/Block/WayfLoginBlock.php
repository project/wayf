<?php

namespace Drupal\wayf\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'WAYF login' block.
 *
 * @Block(
 *   id = "wayf_login_block",
 *   admin_label = @Translation("WAYF login"),
 *   category = @Translation("User")
 * )
 */
class WayfLoginBlock extends BlockBase implements ContainerFactoryPluginInterface {

  public function __construct(array $configuration, $plugin_id, $plugin_definition,
                              protected AccountInterface $currentUser,
  protected ConfigFactoryInterface $configFactory,
  protected FileUrlGeneratorInterface $fileUrlGenerator
  ){
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('config.factory'),
      $container->get('file_url_generator')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf($this->currentUser->isAnonymous());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('wayf.settings');

    $icon = \Drupal::service('extension.path.resolver')->getPath('module', 'wayf') . '/icons/' . $config->get('icon');
    $icon_size = wayf__icon_size($config->get('icon'));
    $uri = $this->fileUrlGenerator->generateAbsoluteString($icon);

    return [
      '#theme' => 'wayf_login',
      '#icon' => [
        'uri' => $uri,
        'size' => $icon_size
      ],
      '#login_url' => '/wayf/consume',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return Cache::PERMANENT;
  }

}
