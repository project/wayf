<?php

namespace Drupal\wayf\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

class WayfSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'wayf_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return [
      'wayf.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;

    $config = $this->config('wayf.settings');

    // IdP settings tab
    $form['vertical_tabs_container'] = [
      '#type' => 'vertical_tabs',
    ];

    $form['idp'] = [
      '#type' => 'details',
      '#title'  => $this->t('WAYF bridge'),
      '#group' => 'vertical_tabs_container',
    ];

    $metadata = wayf__get_ipd_metadata();

    $form['idp']['idp_sso_production'] = [
      '#type' => 'textfield',
      '#attributes' => ['disabled' => 'disabled'],
      '#title' => $this->t('Single signon URL'),
      '#value' => $metadata->sso,
    ];

    $form['idp']['idp_slo_production'] = [
      '#type' => 'textfield',
      '#attributes' => ['disabled' => 'disabled'],
      '#title' => $this->t('Single logout URL'),
      '#value' => $metadata->slo,
    ];

    $form['idp']['idp_certificate_production'] = [
      '#type' => 'textarea',
      '#attributes' => ['disabled' => 'disabled'],
      '#title' => $this->t('Certificate'),
      '#value' => $metadata->cert,
      '#rows' => 18,
    ];

    // Service provider settings tab.
    $form['sp'] = [
      '#type' => 'details',
      '#title'  => $this->t('Service provider'),
      '#group' => 'vertical_tabs_container',
    ];

    $form['sp']['sp_entityid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Connection ID'),
      '#default_value' => $config->get('sp_entityid'),
      '#description' => $this->t('EntityID used for the service.'),
    ];

    $form['sp']['sp_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AssertionConsumerService:0:Location'),
      '#default_value' => $config->get('sp_endpoint'),
      '#description' => $this->t('Endpoint URL for the service.'),
    ];

    $form['sp']['sp_logout_endpoint'] = [
      '#type' => 'textfield',
      '#title' => $this->t('SingleLogoutService redirect location'),
      '#default_value' => $config->get('sp_logout_endpoint'),
      '#description' => $this->t('Endpoint URL for the service.'),
    ];

    $form['sp']['certificate'] = [
      '#type' => 'details',
      '#title' => $this->t('Certificate'),
      '#description' => $this->t('The certificate and private key used to communicate and sign message with WAYF.'),
      '#open' => !((bool) $config->get('sp_key')),
    ];

    $form['sp']['certificate']['sp_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Private key'),
      '#default_value' => $config->get('sp_key'),
      '#description' => $this->t('Private key, base64 PEM formatted. <br>The key should be the data between -----BEGIN RSA PRIVATE KEY----- and -----END RSA PRIVATE KEY-----'),
    ];

    $form['sp']['certificate']['sp_cert'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Certificate'),
      '#default_value' => $config->get('sp_cert'),
      '#description' => $this->t('The certificate should be the data between -----BEGIN CERTIFICATE----- and -----END CERTIFICATE-----'),
    ];

    $form['sp']['organization'] = [
      '#type' => 'details',
      '#title' => $this->t('Organizations information'),
      '#open' => TRUE,
    ];

    $form['sp']['organization']['sp_organizations_list_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Organizations feed URL'),
      '#default_value' => $config->get('sp_organizations_list_url'),
      '#description' => $this->t('The URL of the JSON feed with organizations.'),
    ];

    // Get organization list
    $organization_list = json_decode($config->get('sp_organizations_list'), TRUE);

    // Get languages from the feed.
    $options = [];

    if (!empty($organization_list)) {
      $items = array_keys(reset($organization_list));
      $options = [];
      foreach ($items as $item) {
        if ('schacHomeOrganization' === $item) {
          continue;
        }

        $options[$item] = $item;
      }
    }

    $form['sp']['organization']['sp_organizations_name_language'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('Organizations name language version'),
      '#default_value' => $config->get('sp_organizations_name_language'),
      '#description' => $this->t('The language code of the orginazation names to use. If empty, run cron and make sure the feed URL is correct.'),
    ];


    $form['sp']['organization']['sp_organizations_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OrganizationName'),
      '#default_value' => $config->get('sp_organizations_name'),
    ];

    $form['sp']['organization']['sp_organizations_displayname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OrganizationDisplayName'),
      '#default_value' => $config->get('sp_organizations_displayname'),
    ];

    $form['sp']['organization']['sp_organizations_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OrganizationURL'),
      '#default_value' => $config->get('sp_organizations_url'),
    ];

    $form['sp']['contact'] = [
      '#type' => 'details',
      '#title' => $this->t('Contact information'),
      '#open' => !$config->get('sp_contact_name'),
    ];

    $form['sp']['contact']['sp_contact_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('GivenName'),
      '#default_value' => $config->get('sp_contact_name'),
    ];

    $form['sp']['contact']['sp_contact_mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('EmailAddres'),
      '#default_value' => $config->get('sp_contact_mail'),
    ];

    $form['fields'] = [
      '#type' => 'details',
      '#title'  => $this->t('Field mappings'),
      '#group' => 'vertical_tabs_container',
      '#description' => '<p>' . $this->t('Notice: only textfields can be assigned attributes
        released from WAYF and currently only singular attributes are supported.
        The email address attribute are mapped to user->mail by default.') . '</p>',
    ];

    $mapping = $config->get('mapping');
    $form['fields']['mapping'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $fields_info = \Drupal::service('entity_field.manager')->getFieldDefinitions('user', 'user');
    foreach ($fields_info as $field_name => $field_info) {
      if ('string' === $field_info->getType()) {
        $form['fields']['mapping'][$field_name] = [
          '#type' => 'select',
          '#options' => $this->attribute_options(),
          '#title' => Markup::create($field_info->getLabel()),
          '#default_value' => $mapping[$field_name] ?? null,
        ];
      }
    }

    $form['metadata'] = [
      '#type' => 'details',
      '#title' => $this->t('Metadata (SP)'),
      '#group' => 'vertical_tabs_container',
    ];

    $form['metadata']['container'] = [
      '#type' => 'container',
      '#prefix' => '<div id="idp-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['metadata']['container']['url'] = [
      '#type' => 'markup',
      '#markup' => '<p><strong>Metadata url:</strong> <a target="_blank" href="' . $base_url . '/wayf/metadata">' .  $base_url . '/wayf/metadata' . '</a></p>',
    ];

    $form['metadata']['container']['data'] = [
      '#type' => 'markup',
      '#markup' => '<p>Metadata generated based on the information entered under the "Service provider" tab.</br><div class="sp-metadata"><pre>' . htmlentities(wayf_generate_metadata()) . '</pre></div></p>',
    ];

    $form['display'] = [
      '#type' => 'details',
      '#title'  => $this->t('Display settings'),
      '#group' => 'vertical_tabs_container',
    ];

    $form['display']['alter_login_form'] = [
      '#type' => 'checkbox',
      '#title' => t('Add WAYF login button to the standard login form'),
      '#default_value' => $config->get('alter_login_form'),
    ];

    $icon_path = \Drupal::service('extension.path.resolver')->getPath('module', 'wayf') . '/icons/';
    $icons = [];
    foreach (wayf__icons() as $icon) {
      $uri = \Drupal::service('file_url_generator')->generateAbsoluteString($icon_path . $icon);
      if ($uri) {
        $icons[$icon] = [
          'icon' => new FormattableMarkup('<img src="@uri" alt="WAYF icon" />', ['@uri' => $uri]),
        ];
      }
    }

    $form['display']['icon'] = [
      '#type' => 'tableselect',
      '#multiple' => FALSE,
      '#options' => $icons,
      '#header' => ['icon' => $this->t('Icon')],
      '#default_value' => $config->get('icon'),
    ];

    // Scoping field set.
    $form['scoping'] = [
      '#type' => 'details',
      '#title' => $this->t('Organizations (scoping)'),
      '#group' => 'vertical_tabs_container',
    ];

    if (!empty($organization_list)) {
      $options = [];
      $language = $config->get('sp_organizations_name_language');

      foreach ($organization_list as $entityId => $value) {
        $options[$entityId] = $value[$language];
      }

      asort($options);
    }

    $active_list = $config->get('sp_organizations_active') ? json_decode($config->get('sp_organizations_active'), true) : [];

    $form['scoping']['sp_organizations_active'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Allowed organizations'),
      '#options' => $options,
      '#default_value' => $active_list,
      '#description' => $this->t('Select the organizations whose users should be able to log in. If none are checked, users from everywhere can log in.'),
    ];

    // Settings field set.
    $form['settings'] = [
      '#type' => 'details',
      '#title' => t('Settings'),
      '#group' => 'vertical_tabs_container',
    ];

    $form['settings']['user'] = [
      '#type' => 'fieldset',
      '#title' => t('User creating process'),
      '#description' => t('Select which modules should be used to create the Drupal user after login to WAYF have succesfull completed. Note that order in which the modules are called is by system weight.'),
    ];

    $options = [];
    $hook = 'wayf_create_user';
    \Drupal::moduleHandler()->invokeAllWith($hook, function (callable $hook, string $module) use (&$options) {
      $options[$module] = $module;
    });

    $form['settings']['user']['user_create_modules'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#default_value' => $config->get('user_create_modules'),
    ];

    $form['settings']['redirects'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Redirects'),
    ];

    $form['settings']['redirects']['login_redirect'] = [
      '#type' => 'textfield',
      '#title' => $this->t('The url to redirect to after login.'),
      '#default_value' => $config->get('login_redirect'),
    ];

    $form['settings']['development'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Development'),
    ];

    $form['settings']['development']['development_log_auth_data'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log authentication requests'),
      '#default_value' => $config->get('development_log_auth_data'),
      '#description' => $this->t('Log authentication data including attributes. This can be useful for debugging.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('wayf.settings')
      ->set('idp_sso', $form_state->getValue('idp_sso_production'))
      ->set('idp_slo', $form_state->getValue('idp_slo_production'))
      ->set('idp_certificate', $form_state->getValue('idp_certificate_production'))
      ->set('sp_entityid', $form_state->getValue('sp_entityid'))
      ->set('sp_endpoint', $form_state->getValue('sp_endpoint'))
      ->set('sp_logout_endpoint', $form_state->getValue('sp_logout_endpoint'))
      ->set('sp_key', $form_state->getValue('sp_key'))
      ->set('sp_cert', $form_state->getValue('sp_cert'))
      ->set('sp_organizations_name_language', $form_state->getValue('sp_organizations_name_language'))
      ->set('sp_organizations_name', $form_state->getValue('sp_organizations_name'))
      ->set('sp_organizations_displayname', $form_state->getValue('sp_organizations_displayname'))
      ->set('sp_organizations_url', $form_state->getValue('sp_organizations_url'))
      ->set('sp_contact_name', $form_state->getValue('sp_contact_name'))
      ->set('sp_contact_mail', $form_state->getValue('sp_contact_mail'))
      ->set('mapping',$form_state->getValue('mapping', []))
      ->set('sp_organizations_active', json_encode($form_state->getValue('sp_organizations_active')))
      ->set('alter_login_form', $form_state->getValue('alter_login_form'))
      ->set('icon', $form_state->getValue('icon'))
      ->set('user_create_modules', $form_state->getValue('user_create_modules'))
      ->set('login_redirect', $form_state->getValue('login_redirect'))
      ->set('development_log_auth_data', $form_state->getValue('development_log_auth_data', FALSE))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Function attribute_options returns list of WAYF attributes.
   *
   * @return array
   *   list of singular attributes
   */
  private function attribute_options() {
    return [
      '' => $this->t('Not mapped'),
      'sn'  => $this->t('Last name'),
      'gn' => $this->t('First name'),
      'eduPersonTargetedID'  => $this->t('Nickname'),
      'schacHomeOrganization' => $this->t('Organisation nickname'),
      'eduPersonScopedAffiliation' => $this->t('Primary user affiliation'),
    ];
  }

}
