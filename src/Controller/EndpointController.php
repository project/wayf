<?php

namespace Drupal\wayf\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Url;
use Drupal\wayf\WAYF\SPorto;
use Drupal\wayf\WAYF\SPortoException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class EndpointController extends ControllerBase  {

  /**
   * @var LoggerChannelFactoryInterface
   */
  protected  $loggerFactory;

  /**
   * @var RequestStack
   */
  protected $requestStack;

  protected $session;

  public function __construct(
    RequestStack $requestStack,
    LoggerChannelFactoryInterface $loggerFactory,
    SessionInterface $session
  ) {
    $this->requestStack = $requestStack;
    $this->loggerFactory = $loggerFactory;
    $this->session = $session;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack'),
      $container->get('logger.factory'),
      $container->get('session')
    );
  }
  /**
   * Handle login redirect from WAYF.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  function consume() {
    // Get SAML response from the POST data.
    $SAMLResponse = $this->requestStack->getCurrentRequest()->get('SAMLResponse');

    // Load configuration.
    $config = $this->config('wayf.settings');
    $cert = trim(preg_replace('/\s+/', ' ', $config->get('idp_certificate')));
    $private_key = trim(preg_replace('/\s+/', ' ', $config->get('sp_key')));

    $sportoConfig = [
		  'idp_certificate' => $cert,
		  'sso' => $config->get('idp_sso'),
		  'private_key' => $private_key,
		  'asc' => $config->get('sp_endpoint'),
		  'entityid' => $config->get('sp_entityid'),
    ];

    // Get scopes based on allowed organizations.
    $scopes = array_filter(json_decode($config->get('sp_organizations_active'), true));

    try {
      // Send request.
      $sporto = new SPorto($sportoConfig);
      $result = $sporto->redirect($SAMLResponse, $scopes);
    }
    catch (SPortoException $e) {
      $this->messenger()->addError($this->t('Login failed. Please contact the site administrator or try again.'));
      $this->loggerFactory->get('wayf')->error($e->getMessage());

      return $this->redirect('<front>');
    }

    if ($config->get('development_log_auth_data')) {
      $this->loggerFactory->get('wayf')->debug('Authentication data: %data', [
          '%data' => var_export($result, TRUE),
        ]
      );
    }

    // User the selected login hook (hook_wayf_create_user) to process
    // the login.
    $hooks = $config->get('user_create_modules');
    foreach ($hooks as $module) {
      \Drupal::moduleHandler()->invoke($module, 'wayf_create_user', [$result['attributes']]);
    }

    return (new RedirectResponse($config->get('login_redirect')));
  }

  /**
   * Logout of WAYF.
   */
  public function logout() {
    // Load configuration.
    $config = $this->config('wayf.settings');

    // Set library configuration.
    $sportoConfig = [
      'idp_certificate' => $config->get('idp_certificate'),
      'sso' => $config->get('idp_sso'),
      'slo' => $config->get('idp_slo'),
      'private_key' => $config->get('sp_key'),
      'asc' => $config->get('sp_endpoint'),
      'entityid' => $config->get('sp_entityid'),
    ];

    try {
      // Build SAML message and logout.
      $sporto = new SPorto($sportoConfig);

      // Check if user is logged into WAYF.
      if ($sporto->isLoggedIn()) {
        // Give other an change to clean up.
        \Drupal::moduleHandler()->invokeAll('wayf_pre_logout');

        // Store destination if set before redirect.
        $destination = \Drupal::destination()->get();
        if (!empty($destination) && $destination != Url::fromRoute('wayf.logout')->toString()) {
          $this->session->set('wayf_destination', $destination);
        }

        // Send logout message.
        $sporto->logout();
      }
      else {
        // Check if the user is logged into the site. The WAYF logout redirect may
        // have stopped the logout process. So this will give Drupal a change to
        // complete the logout.
        if (\Drupal::currentUser()->isAuthenticated()) {
          return $this->redirect('user.logout');
        }

        // Get destination from session.
        $destination = $this->session->get('wayf_destination');
        if (!empty($destination)) {
          // Clean up session.
          $this->session->remove('wayf_destination');

          // Redirect.
          return (new RedirectResponse($destination))->send();
        }
      }
    }
    catch (SPortoException $e) {
      $this->messenger()->addError($this->t('Logout failed. Please contact the site administrator or try again.'));
      $this->loggerFactory->get('wayf')->error($e->getMessage());
    }

    return $this->redirect('<front>');
  }

  /**
   * Generate metadata response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   */
  public function metadata() {
    $response = new Response(wayf_generate_metadata());
    $response->headers->set('Content-Type', 'text/xml');

    return $response;
  }
}
